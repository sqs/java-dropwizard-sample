package com.example.helloworld.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.fest.assertions.api.Assertions.assertThat;

public class PersonTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {
        Person person = new Person();
        person.setId(1);
        person.setFullName("Billy");
        person.setJobTitle("Guy");
        assertThat(MAPPER.readValue(fixture("fixtures/person.json"), Person.class))
                .isEqualsToByComparingFields(person);
    }
}