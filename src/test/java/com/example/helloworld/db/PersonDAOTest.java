package com.example.helloworld.db;

import com.example.helloworld.core.Person;
import com.example.helloworld.utils.LogMonitor;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersonDAOTest {
    private LogMonitor logMonitor;
    private static final SessionFactory sessionFactory = new MockSessionFactory();
    private static final PersonDAO personDAO = new PersonDAO(sessionFactory);

    @Before
    public void setup() {
        logMonitor = new LogMonitor(MockSession.class);
    }

    @Test
    public void testFindById() throws Exception {
        personDAO.findById(1l);
        assertEquals("[INFO] Looking for Person with ID 1", logMonitor.getMessages().get(0));
    }

    @Test
    public void testCreate() throws Exception {
        Person dude = new Person();
        dude.setFullName("William Tell");
        dude.setJobTitle("Archer");
        personDAO.create(dude);
        assertEquals("[INFO] Saving or updating something: William Tell, Archer", logMonitor.getMessages().get(0));
    }

    @Test
    public void testDelete() throws Exception {
        Person dude = new Person();
        dude.setFullName("William Tell");
        dude.setJobTitle("Archer");
        dude.setId(1l);
        personDAO.delete(dude);
        assertEquals("[INFO] Deleting William Tell, Archer", logMonitor.getMessages().get(0));
    }

    @Test
    public void testFindAll() throws Exception {
        personDAO.findAll();
        assertEquals("[INFO] Using named query: com.example.helloworld.core.Person.findAll", logMonitor.getMessages().get(0));
    }
}