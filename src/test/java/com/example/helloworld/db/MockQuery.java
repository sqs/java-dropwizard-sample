package com.example.helloworld.db;


import org.hibernate.*;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

public class MockQuery implements Query {
    @Override
    public FlushMode getFlushMode() {
        return null;
    }

    public int executeUpdate() {
        return 0;
    }

    public Integer getFirstResult() {
        return null;
    }

    public Integer getMaxResults() {
        return null;
    }

    public Iterator iterate() {
        return null;
    }

    public List list() {
        return null;
    }

    public LockOptions getLockOptions() {
        return null;
    }

    public Object uniqueResult() {
        return null;
    }

    public Query addQueryHint(String var1) {
        return this;
    }

    public Query setBigDecimal(int var1, BigDecimal var2) {
        return this;
    }

    public Query setBigDecimal(String var1, BigDecimal var2) {
        return this;
    }

    public Query setBigInteger(int var1, BigInteger var2) {
        return this;
    }

    public Query setBigInteger(String var1, BigInteger var2) {
        return this;
    }

    public Query setBinary(int var1, byte[] var2) {
        return this;
    }

    public Query setBinary(String var1, byte[] var2) {
        return this;
    }

    public Query setBoolean(int var1, boolean var2) {
        return this;
    }

    public Query setBoolean(String var1, boolean var2) {
        return this;
    }

    public Query setByte(int var1, byte var2) {
        return this;
    }

    public Query setByte(String var1, byte var2) {
        return this;
    }

    public Query setCacheable(boolean var1) {
        return this;
    }

    public Query setCacheMode(CacheMode var1) {
        return this;
    }

    public Query setCacheRegion(String var1) {
        return this;
    }

    public Query setCalendar(int var1, Calendar var2) {
        return this;
    }

    public Query setCalendar(String var1, Calendar var2) {
        return this;
    }

    public Query setCalendarDate(int var1, Calendar var2) {
        return this;
    }

    public Query setCalendarDate(String var1, Calendar var2) {
        return this;
    }

    public Query setCharacter(int var1, char var2) {
        return this;
    }

    public Query setCharacter(String var1, char var2) {
        return this;
    }

    public Query setComment(String var1) {
        return this;
    }

    public Query setDate(int var1, Date var2) {
        return this;
    }

    public Query setDate(String var1, Date var2) {
        return this;
    }

    public Query setDouble(int var1, double var2) {
        return this;
    }

    public Query setDouble(String var1, double var2) {
        return this;
    }

    public Query setEntity(int var1, Object var2) {
        return this;
    }

    public Query setEntity(String var1, Object var2) {
        return this;
    }

    public Query setFetchSize(int var1) {
        return this;
    }

    public Query setFirstResult(int var1) {
        return this;
    }

    public Query setFloat(int var1, float var2) {
        return this;
    }

    public Query setFloat(String var1, float var2) {
        return this;
    }

    public Query setFlushMode(FlushMode var1) {
        return this;
    }

    public Query setInteger(int var1, int var2) {
        return this;
    }

    public Query setInteger(String var1, int var2) {
        return this;
    }

    public Query setLocale(int var1, Locale var2) {
        return this;
    }

    public Query setLocale(String var1, Locale var2) {
        return this;
    }

    public Query setLockMode(String var1, LockMode var2) {
        return this;
    }

    public Query setLockOptions(LockOptions var1) {
        return this;
    }

    public Query setLong(int var1, long var2) {
        return this;
    }

    public Query setLong(String var1, long var2) {
        return this;
    }

    public Query setMaxResults(int var1) {
        return this;
    }

    public Query setParameter(int var1, Object var2, Type var3) {
        return this;
    }

    public Query setParameter(int var1, Object var2) {
        return this;
    }

    public Query setParameter(String var1, Object var2, Type var3) {
        return this;
    }

    public Query setParameter(String var1, Object var2) {
        return this;
    }

    public Query setParameterList(String var1, Collection var2, Type var3) {
        return this;
    }

    public Query setParameterList(String var1, Collection var2) {
        return this;
    }

    public Query setParameterList(String var1, Object[] var2, Type var3) {
        return this;
    }

    public Query setParameterList(String var1, Object[] var2) {
        return this;
    }

    public Query setParameters(Object[] var1, Type[] var2) {
        return this;
    }

    public Query setProperties(Map var1) {
        return this;
    }

    public Query setProperties(Object var1) {
        return this;
    }

    public Query setReadOnly(boolean var1) {
        return this;
    }

    public Query setResultTransformer(ResultTransformer var1) {
        return this;
    }

    public Query setSerializable(int var1, Serializable var2) {
        return this;
    }

    public Query setSerializable(String var1, Serializable var2) {
        return this;
    }

    public Query setShort(int var1, short var2) {
        return this;
    }

    public Query setShort(String var1, short var2) {
        return this;
    }

    public Query setString(int var1, String var2) {
        return this;
    }

    public Query setString(String var1, String var2) {
        return this;
    }

    public Query setText(int var1, String var2) {
        return this;
    }

    public Query setText(String var1, String var2) {
        return this;
    }

    public Query setTime(int var1, Date var2) {
        return this;
    }

    public Query setTime(String var1, Date var2) {
        return this;
    }

    public Query setTimeout(int var1) {
        return this;
    }

    public Query setTimestamp(int var1, Date var2) {
        return this;
    }

    public Query setTimestamp(String var1, Date var2) {
        return this;
    }

    public Type[] getReturnTypes() {
        return null;
    }

    public String getCacheRegion() {
        return null;
    }

    public Integer getTimeout() {
        return null;
    }

    public Integer getFetchSize() {
        return null;
    }

    public CacheMode getCacheMode() {
        return null;
    }

    public boolean isReadOnly() {
        return false;
    }

    public boolean isCacheable() {
        return false;
    }

    public ScrollableResults scroll() {
        return null;
    }

    public ScrollableResults scroll(ScrollMode var1) {
        return null;
    }

    public String getComment() {
        return null;
    }

    public String getQueryString() {
        return null;
    }

    public String[] getNamedParameters() {
        return null;
    }

    public String[] getReturnAliases() {
        return null;
    }
}