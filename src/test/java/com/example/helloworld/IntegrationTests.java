package com.example.helloworld;

import com.example.helloworld.core.Saying;
import com.google.common.base.Optional;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.ClassRule;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class IntegrationTests {
    @ClassRule
    public static final DropwizardAppRule<HelloWorldConfiguration> RULE = new DropwizardAppRule<>(HelloWorldApplication.class, "example.yml");

    @Test
    public void testHelloWorld() throws Exception {
        final Optional<String> NAME = Optional.fromNullable("Dr. IntegrationTest");
        final String RESOURCE_PATH = String.format("http://localhost:%s%s", RULE.getLocalPort(), "/api/hello-world");

        Client client = new Client();
        ClientResponse response = client.resource(RESOURCE_PATH).queryParam("name", NAME.get()).get(ClientResponse.class);

        assertEquals(200, response.getStatus());
        assertEquals("Hello, Dr. IntegrationTest!", response.getEntity(Saying.class).getContent());
    }
}